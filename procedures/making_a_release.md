## Making an SDK release

Okay, this is just me making notes as a I do the SDK 1.1.0 release.


### Auditing Our 70+ repos

 - "Staging" has been good for a while, the SDK spreadsheet has been updated, everything has been tagged.

 - make a new file publishing-tools/other-tools/sdk_csvs/sdk-x.y.z.csv containing the csv generated at the bottom of the voxl sdk spreadsheet

 - go into the other tools directory and run the release tool in test mode

```
cd other-tools
./release-sdk.sh --file sdk_csvs/sdk-1.1.0.csv --test
```

 - you're going to be given a list of safety check results. Go through all of them and make sure the spreadsheet and the repos match. Check for people pushing commits straight to master when they shouldn't, etc.

 - If you updated the spreadsheet in the last step (you probably needed to), update the CSV and re-run the test mode with the --pull argument to make sure any commits and tags you just made get pulled down.

```
./release-sdk.sh --file sdk_csvs/sdk-1.1.0.csv --test --pull
```

Rinse, Repeat. Make note of any packages that have warnings but that you are okay with.


### Update CI Status Page


Now you have an updated CSV you are confident in, we can use that to update our ci-status repo which exists here:

https://gitlab.com/voxl-public/voxl-sdk/ci-status

While you are still in the publishing-tools/other-tools directory, run the update script:

```
./update-ci-status-page.sh -f sdk_csvs/sdk-1.1.0.csv --sdk sdk-1.1.0
```


This will automatically do the git pull/commit/push for the ci-status repo and generate a new README that's shown when to go to the above URL. This is helpful to get the new SDK-X.Y.Z column on that status page so we can all watch as CI builds the tags for the upcomming release.



### Make a new distro on our debian package repo


make a new folder on the repo vm and add that folder to the config file

 ```
 gcloud login
 sudo su
 cd /debian/dists
 mkdir -p apq8096/sdk-1.1/binary-arm64/
 mkdir -p qrb5165/sdk-1.1/binary-arm64/
 mkdir -p fc-v1/sdk-1.1/
 mkdir -p fc-v2/sdk-1.1/
cd ../
nano config/apt-ftparchive.conf (add SDK-1.1 to bottom, potentially remove old ones)
```

- update the repo to make a new packages.gz

```
cd /debian/
./update-deb-repo.sh
```



### Do a test tag

voxl-px4 is a good candiate for this since it also builds for flight core V1 and V2 so it has the most chances to go wrong. Also, if there are any messed up tags where someone pushed to master after the code freeze, you will need to tag these manually before the script runs, otherwise it will give an SDK tag to the latest on master.

go a gittag sdk-1.1.0 (3 digits always!!) on your test repo then check the following things:

 1. CI passed
 2. package ended up in the http://voxl-packages.modalai.com/dists/qrb5165/sdk-1.1/binary-arm64/ repo (also for APQ, anf FC-V1 FC-V2)
 3. the ci-status page picked up the correct SDK job pass (https://gitlab.com/voxl-public/voxl-sdk/ci-status)


### Update KNOWN_IN_SYS_IMG and PRIVATE_PROJECTS lists in the release-sdk-script

the script will tell you when it finds packages to add to the KNOWN_IN_SYS_IMG list, but you should sanity check them first.

Also add any new private projects to the other list, or the tool will get stuck when it hits those.


### Install python dependencies for the release tool

```
sudo apt install python3 python-is-python3
pip3 install numpy cairosvg opencv-python
```

### Compile a new package for everything fresh

This will take a whole day. You will run into many little build errors and quirks. but that's okay, that's the whole point of this process is to find dependency loops and stuff that was hidden in the dev and staging repos.

```
./release-sdk.sh --file sdk_csvs/sdk-1.1.0.csv  --pull
```


### Make a new voxl-suite

voxl-suite builds entirely based on tags. It makes a new package based off of what packages are in a given sdk-X.Y deb repo

for the first one, do a "beta1" and make some sdk installers to test first before a final tag

```
cd voxl-suite/
gittag sdk-1.1.0-beta1
```

Make sure it shows up in the voxl-packages repos for apq and qrb.


### Choose a System Image


### make some tarballs

do all 4 platforms, instructions on the screen.

SDK version question should be 1.1
version string should be a beta: 1.1.0-beta1

```
cd ../qrb5165
./make_sdk_release.sh
```

```
cd ../apq8096
./make_sdk_release.sh
```



### test

test, make sure all 4 work


### final release

Now redo the voxl-suite tag, but with no beta in the name, just sdk-x.y.0

```
cd voxl-suite/
gittag sdk-1.1.0
```

now redo 4 platforms, instructions on the screen.

SDK version question should be 1.1
version string should NOT be a beta: 1.1.0

```
cd publishing-tools/qrb5165
./make_sdk_release.sh
```

```
cd ../apq8096
./make_sdk_release.sh
```


### update spreadsheet

The `make_sdk_release.sh` script's main purpose was to go and put an sdk-x.y.z tag on every package. Now that's done we can check the entire "sdk tag" column on the last speadsheet sheet.

Duplicate the last sheet and rename from let's say 1.1.0 to 1.1.1

uncheck all the boxes in the "new" column for this new sheet. When a package is being staged for this next release, make sure to uncheck "version tag" and "SDK tag" for the package until it's pasted I&T and actually tagged into staging and the stable repo.


### Make Release Notes


#### start new file

Now to update the public docs with a changelog. Start by pulling down the repo and updating a new page.

```
cd ~/git/
git clone git@gitlab.com:voxl-public/support/documentation.git
cd documentation/docs/05-voxl-sdk/02-release-notes/
cp 50-SDK-1.0.md 49-SDK-1.1.md
```

#### generate a version table

This will use the CSV you created from the spreadsheet earlier

```
cd publishing-tools/other-tools/
./generate-change-table.sh sdk_csvs/sdk-1.0.0.csv sdk_csvs/sdk-1.1.0.csv -p -m
```

copy this into your documentation markdown


#### Make a complete changelog


Generate a new changelog by using the changelog generator python tool. Start by following the instructions in publishing-tools/sdk-changelog-generator/README.md

```
## install dependencies
cd publishing-tools/sdk-changelog-generator/
sudo apt update
sudo apt install -y python3-tk
pip3 install -r requirements.txt
```

don't forget to set a gitlab token, then launch the UI

```
./main.py
```


select qrb5165 in the top dropdown. Then sdk-1.0 (old) in the left and sdk-1.1 on the right. Pick the stable voxl-suite.deb package name for each. Then hit compare, you should quickly see a full version table.

Now hit "export detailed changelog", this will be a slow process, be patient. It's going to pull the changelog from every single package and make a single file from it.

Copy the text from the file it just generated into the public docs markdown and trim/polish as you see fit. Put the text in a code block so the formatting doesn't get messed up.

#### Finally, tag this publishing-tools repo

You probably made some fixes and updates along the way, so don't forget to commit and tag this repo to mark the point where these tools were able to build the SDK.
