import numpy as np
import requests
from io import BytesIO
from PIL import Image
from cairosvg import svg2png
import cv2
import sys

def main():

    try:

        svg_data = requests.get(sys.argv[1]).content

        png = svg2png(bytestring=svg_data)

        pil_img = Image.open(BytesIO(png)).convert('RGBA')
        img = cv2.cvtColor(np.array(pil_img), cv2.COLOR_RGBA2BGRA)

        average = img.mean(axis=0).mean(axis=0)

        int_average = [int(average) for average in average]

        # f = open("nums.txt", "a")
        # f.write(sys.argv[1])
        # f.write("\n")
        # f.write(" " + str(int_average[0]))
        # f.write(" " + str(int_average[1]))
        # f.write(" " + str(int_average[2]))
        # f.write("\n")
        # f.close()

        # Green
        if int_average[0] == 44 and int_average[1] == 192 and int_average[2] == 85 :
            print("3")
            return

        # Yellow (Running)
        if int_average[0] == 49 and int_average[1] == 174 and int_average[2] == 209 :
            print("2")
            return

        # Yellow (Pending)
        if int_average[0] == 52 and int_average[1] == 174 and int_average[2] == 208 :
            print("2")
            return

        # Red
        if int_average[0] == 82 and int_average[1] == 103 and int_average[2] == 209 :
            print("1")
            return

        print("0")
        return

    except:
        print("0")
        return

main()
