#! /bin/bash


FILE_NAME=""

. lib.sh

# List of control dependencies that we trust will be satisfied
KNOWN_IN_SYS_IMG=(
    "docker"
    "libpng-dev"
    "voxl-system-tweaks"
    "lib32-libpmd"
    "libvoxl-io"
    "cci-direct-support"
    "libboost-filesystem-dev"
    "python3-numpy"
    "python3-serial"
    "voxl2-system-image"
    "libpng16-16")

# Private projects where we won't be able to see the ci badge (annoying)
PRIVATE_PROJECTS=(
    "libmodal-cv"
    "voxl-open-vins"
    "libfc-sensor"
    "modalai-slpi"
    "voxl-vtx"
    "libslpi-link"
    "voxl-fpv-px4"
    "voxl-state-estimator"
    "voxl-lepton-tracker"
)

PULL_PROJECTS=false

declare -A BUILD_LIST

__validate_url(){

  wget -S --spider $1  2>&1 | grep -q 'HTTP/1.1 200 OK' && return 0
  return 1
}

__print_usage () {
    echo "Command line arguments are as follows:"
    echo "-c, --clean             : Clean the build relics (won't run main)"
    echo "-h, --help              : Print this help message"
    echo "-f, --file {file.csv}   : Specify the project list csv to load"
    echo "-l, --list-extra        : Generate a list of repos with excess branches (not matching master|dev)"
    echo "-p, --pull              : Pull updates to projects that are already downloaded"
    echo "-t, --test              : Test projects, only run safety checks, don't do any tagging"
    echo
}

__parse_opts(){

    while [[ $# -gt 0 ]]; do
        case $1 in
            -c|--clean)
                __clean
                __exit 0
                ;;
            -h|--help)
                # Make sure we don't purge the relics on a false run
                __print_usage
                __exit 0
                ;;
            -f|--file)
                if ! [[ $# -gt 1 ]] ; then
                    echo "file option needs an option"
                    exit 1
                fi
                FILE_NAME=$2
                shift
                ;;
            -l|--list-extra)
                MODE=LIST_BRANCHES
                ;;
            -p|--pull)
                PULL_PROJECTS=true
                ;;
            -t|--test)
                MODE=TEST
                ;;
            *)
                # Make sure we don't purge the relics on a false run
                echo "Unknown argument $1"
                __print_usage
                __exit 1
                ;;
        esac
        shift
    done

    if [ "${FILE_NAME}" == "" ]; then
        echo "please provide a file name"
        __print_usage
        exit 1
    fi
}

__list_extra_branches(){

    local DEV_CHANGE_LIST=()

    echo
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Excess Branches----${RESET_ALL}"

    local i
    for i in ${!PROJECT_LIST[@]} ; do

        if ! $MAIN_RUNNING ; then
            __exit
        fi

        local PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        local PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        cd "${REPO_DIR}/${PROJ_NAME}"

        if [[ ( "$(git branch -r | egrep -v "origin/master$|origin/dev$" | wc -l)" > 0  ) ]]; then
            echo -e "$CLR_LIT_YLW----$PROJ_NAME----$RESET_ALL"
            git branch -r | egrep -v "origin/master$|origin/dev$" | sed "s-origin/--g"
        fi

        if ! [[ "$(git show-ref | grep origin/dev$ | cut -d ' ' -f 1)" == "$(git show-ref | grep origin/master$ | cut -d ' ' -f 1)" ]] ; then
            # Make sure the filesystem is different too, not just the refs
            if [[ "$( git diff "$(git show-ref | grep origin/dev$ | cut -d ' ' -f 1)"..."$(git show-ref | grep origin/master$ | cut -d ' ' -f 1)" )" == "" ]] ; then
                DEV_CHANGE_LIST+=("${PROJ_NAME}")
            fi

        fi

    done

    if [[ ${#DEV_CHANGE_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_GRN}${SET_BOLD}----Branches with different dev/master----${RESET_ALL}"
        echo ${DEV_CHANGE_LIST[@]} | tr ' ' '\n' | sort | column
    fi

}

__check_qrb5165_repo() {

    cd "${REPO_DIR}/${1}"
    ! cat .gitlab-ci.yml | grep -v "#" | grep -q "qrb5165" && return 0

    local PROJ_LINE=$(echo ${PROJECT_LIST[@]} | tr ' ' '\n' | grep "$1,")

    local PROJ_NAME=$(echo $PROJ_LINE | cut -d ',' -f 1)
    local PROJ_VER=$(echo $PROJ_LINE | cut -d ',' -f 2)

    local url="http://voxl-packages.modalai.com/dists/qrb5165/${RELEASE_NAME_SHORT}/binary-arm64/${PROJ_NAME}_${PROJ_VER}_arm64.deb"

    __validate_url $url && return 0

    return 1
}

__check_apq8096_repo() {

    cd "${REPO_DIR}/${1}"
    ! cat .gitlab-ci.yml | grep -v "#" | grep -q "apq8096" && return 0

    local PROJ_LINE=$(echo ${PROJECT_LIST[@]} | tr ' ' '\n' | grep "$1,")

    local PROJ_NAME=$(echo $PROJ_LINE | cut -d ',' -f 1)
    local PROJ_VER=$(echo $PROJ_LINE | cut -d ',' -f 2)

    local url="http://voxl-packages.modalai.com/dists/apq8096/${RELEASE_NAME_SHORT}/binary-arm64/${PROJ_NAME}_${PROJ_VER}.ipk"

    __validate_url $url && return 0

    return 1
}

__is_package_ready() {

    __check_apq8096_repo $1 && __check_qrb5165_repo $1 && return 0
    return 1
}

__check_pipeline_status (){

    local PROJ_NAME=$1
    local PROJ_DIR=$( echo ${PROJECT_LIST[@]} | tr ' ' '\n' | grep "$1," | cut -d ',' -f 3 )

    # sample URL:
    # Set the key to I and the width to 1 to mostly get rid of it
    # https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io/badges/master/pipeline.svg?key_text=I&key_width=1
    local url="https://gitlab.com/voxl-public/voxl-sdk/${PROJ_DIR}/${PROJ_NAME}/badges/${RELEASE_NAME}/pipeline.svg?key_text=I&key_width=1"
    cd ${HOME_DIR}
    __validate_url $url && python ./get-pipeline-status.py $url $1  | tr -d '\n'
}

__perform_safety_checks() {

    local ERROR_CHGLG_LIST=()
    local ERROR_CHGLG_MISS_LIST=()
    local ERROR_CTRL_LIST=()
    local ERROR_VER_MISMATCH_LIST=()
    local ERROR_MISS_VER_TAG_LIST=()
    local ERROR_REF_LIST=()
    local WARNING_README_LIST=()
    local WARNING_REF_LIST=()
    local DEV_CHANGE_LIST=()

    local ERROR=0

    echo
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Performing Safety Checks----${RESET_ALL}"
    for i in ${!PROJECT_LIST[@]} ; do


        __print_progress_bar ${i} "${#PROJECT_LIST[@]}"

        PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        PROJ_VER=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 2)
        PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        # For the mode where we run safety checks anyways, ignore the failed projects
        if [[ "${ERROR_CLONE_LIST[*]}" =~ "$PROJ_NAME" ]] ; then
            continue
        fi


        cd "$REPO_DIR/$PROJ_NAME"

        if ! [ -f "pkg/control/control" ] ; then
            ERROR=1
            ERROR_CTRL_LIST+=("${PROJ_NAME}")
            for j in ${!SAFE_LIST[@]} ; do
                if [ " ${SAFE_LIST[j]} " == " $PROJ_NAME " ] ; then
                    unset SAFE_LIST[j]
                    break
                fi
            done
            SAFE_LIST=("${SAFE_LIST[@]}")
        elif ! [[ "$(cat pkg/control/control | grep Version | cut -d ' ' -f 2)" == "${PROJ_VER}" ]] ; then
            ERROR=1
            ERROR_VER_MISMATCH_LIST+=("${PROJ_NAME}")
            for j in ${!SAFE_LIST[@]} ; do
                if [ " ${SAFE_LIST[j]} " == " $PROJ_NAME " ] ; then
                    unset SAFE_LIST[j]
                    break
                fi
            done
            SAFE_LIST=("${SAFE_LIST[@]}")
        fi

        if ! git show-ref | grep -q "tags/v${PROJ_VER}" ; then
            ERROR=1
            ERROR_MISS_VER_TAG_LIST+=("${PROJ_NAME}")
            for j in ${!SAFE_LIST[@]} ; do
                if [ " ${SAFE_LIST[j]} " == " $PROJ_NAME " ] ; then
                    unset SAFE_LIST[j]
                    break
                fi
            done
            SAFE_LIST=("${SAFE_LIST[@]}")
        elif ! [[ "$(git show-ref | grep "tags/v${PROJ_VER}" | cut -d ' ' -f 1)" == "$(git show-ref | grep origin/HEAD | cut -d ' ' -f 1)" ]] ; then

            # If the refs are different but the filesystem is identical just flash a warning
            if [[ "$( git diff "$(git show-ref | grep "tags/v${PROJ_VER}" | cut -d ' ' -f 1)"..."$(git show-ref | grep origin/HEAD | cut -d ' ' -f 1)" )" == "" ]] ; then
                WARNING_REF_LIST+=("${PROJ_NAME}")

            # If the filesystem is different but only the readme/changelog changed just flash a warning
            elif [[ "$( git diff "$(git show-ref | grep "tags/v${PROJ_VER}" | cut -d ' ' -f 1)"..."$(git show-ref | grep origin/HEAD | cut -d ' ' -f 1)" ':(exclude)README.md' ':(exclude)CHANGELOG' ':(exclude).gitlab-ci.yml')" == "" ]] ; then
                WARNING_README_LIST+=("${PROJ_NAME}")

            # Actual Issue, master contains different code than the expected version
            else
                ERROR=1
                ERROR_REF_LIST+=("${PROJ_NAME}")

                for j in ${!SAFE_LIST[@]} ; do
                    if [ " ${SAFE_LIST[j]} " == " $PROJ_NAME " ] ; then
                        unset SAFE_LIST[j]
                        break
                    fi
                done
                SAFE_LIST=("${SAFE_LIST[@]}")
            fi
        fi

        # Check for chagelog issues only if everything elese is good to go
        if [ $ERROR -eq 0 ] && ! cat CHANGELOG 2>/dev/null | grep -q $PROJ_VER ; then
            if ! [ -f "CHANGELOG" ] ; then
                ERROR_CHGLG_LIST+=("${PROJ_NAME}")
            else
                ERROR_CHGLG_MISS_LIST+=("${PROJ_NAME}")
            fi

            ERROR=1
            for j in ${!SAFE_LIST[@]} ; do
                if [ " ${SAFE_LIST[j]} " == " $PROJ_NAME " ] ; then
                    unset SAFE_LIST[j]
                    break
                fi
            done
            SAFE_LIST=("${SAFE_LIST[@]}")
        fi

        # Check to see if there are any projects with unmerged changes on dev
        if ! [[ "$(git show-ref | grep origin/dev$ | cut -d ' ' -f 1)" == "$(git show-ref | grep origin/master$ | cut -d ' ' -f 1)" ]] ; then
            # Make sure the filesystem is different too, not just the refs
            if [[ "$( git diff "$(git show-ref | grep origin/dev$ | cut -d ' ' -f 1)"..."$(git show-ref | grep origin/master$ | cut -d ' ' -f 1)" )" == "" ]] ; then
                DEV_CHANGE_LIST+=("${PROJ_NAME}")
            fi

        fi

        cd $HOME_DIR

    done

    __print_progress_bar 1 1
    echo

    if [ $ERROR -ne 0 ] ; then
        echo -e "${CLR_LIT_RED}${SET_BOLD}----Projects Failed Safety Checks----${RESET_ALL}"
    fi

    if [[ ${#ERROR_CTRL_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages are missing 'pkg/control/control' files:${RESET_ALL}"
        echo ${ERROR_CTRL_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#ERROR_VER_MISMATCH_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages have a different control version than the one specified in the CSV:${RESET_ALL}"
        echo ${ERROR_VER_MISMATCH_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#ERROR_MISS_VER_TAG_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages are missing a version tag matching the one specified in the CSV:${RESET_ALL}"
        echo ${ERROR_MISS_VER_TAG_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#ERROR_REF_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages master branch is not at the same point as the version tag:${RESET_ALL}"
        echo ${ERROR_REF_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#ERROR_CHGLG_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages are missing 'CHANGELOG' files:${RESET_ALL}"
        echo ${ERROR_CHGLG_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#ERROR_CHGLG_MISS_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages do not have a 'CHANGELOG' entry for the current release:${RESET_ALL}"
        echo ${ERROR_CHGLG_MISS_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#WARNING_README_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_YLW}${SET_BOLD}The following packages master branch is not at the same point as the version tag (but only README/CHANGELOG/.gitlab-ci.yml changed):${RESET_ALL}"
        echo ${WARNING_README_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#WARNING_REF_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_YLW}${SET_BOLD}The following packages master/tag refs are different but fs is the same (this is a git ref issue):${RESET_ALL}"
        echo ${WARNING_REF_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#DEV_CHANGE_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_YLW}${SET_BOLD}The following packages have unmerged changes on dev:${RESET_ALL}"
        echo ${DEV_CHANGE_LIST[@]} | tr ' ' '\n' | sort | column

    fi

    if [[ ${#SAFE_LIST[@]} -gt 0 ]] ; then
        echo
        echo -e "${CLR_LIT_GRN}${SET_BOLD}The following packages encountered no errors:${RESET_ALL}"
        echo ${SAFE_LIST[@]} | tr ' ' '\n' | sort | column
    fi

    if [ $ERROR -ne 0 ] && [[ "$MODE" != "TEST" ]]; then
        echo ""
        read -p "Do you accept these warnings and wish to continue?(y/n)? "
        echo
        if ! [[ $REPLY =~ ^[Yy]$ ]] ; then
            echo "exiting"
            __exit 1
        fi
        echo "carrying on"
    fi

    if [[ ${#ERROR_CLONE_LIST[@]} -gt 0 ]] ; then
        __exit 1
    fi

    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Succcess----${RESET_ALL}"
    echo
}

__prompt_release_name() {

    echo "Safety checks passed, ready to proceed to tagging"
    echo "Enter the name of the release (should follow the structure \"sdk-X.Y.0\")"
    echo -en "${SET_BOLD}Name: ${RESET_ALL}"

    read RELEASE_NAME

    RELEASE_NAME_SHORT=${RELEASE_NAME%.*} ## trim off last patch number
    echo
}

# __configure_gcloud_repo() {

#     [ -f ${TMP_DIR}/gcloud-cmd.txt ] && rm ${TMP_DIR}/gcloud-cmd.txt
#     touch ${TMP_DIR}/gcloud-cmd.txt

#     # Ensure the apq8096 folder is there and clean it if it already existed
#     echo "sudo mkdir -p /debian/dists/apq8096/${RELEASE_NAME}/binary-arm64/" >> ${TMP_DIR}/gcloud-cmd.txt
#     #echo "sudo rm -rf /debian/dists/apq8096/${RELEASE_NAME}/binary-arm64/*" >> ${TMP_DIR}/gcloud-cmd.txt

#     # Ensure the qrb5165 folder is there and clean it if it already existed
#     echo "sudo mkdir -p /debian/dists/qrb5165/${RELEASE_NAME}/binary-arm64/" >> ${TMP_DIR}/gcloud-cmd.txt
#     #echo "sudo rm -rf /debian/dists/qrb5165/${RELEASE_NAME}/binary-arm64/*" >> ${TMP_DIR}/gcloud-cmd.txt

#     # Add the release to the apt config file if it wasn't already there
#     echo "! cat /debian/config/apt-ftparchive.conf | grep -q ${RELEASE_NAME} &&" >> ${TMP_DIR}/gcloud-cmd.txt
#     echo "sudo sed -i '/  Sections/ s/\";/ ${RELEASE_NAME}\";/g' /debian/config/apt-ftparchive.conf" >> ${TMP_DIR}/gcloud-cmd.txt

#     # Run the chain of commands
#     gcloud compute ssh --project modalai-core-services --zone us-west2-a modalai-vm-package-repo < ${TMP_DIR}/gcloud-cmd.txt
# }

__build_dependency_structure() {

    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Building Dependency Structure----${RESET_ALL}"

    declare -A depsMap

    MISSING_DEPS_LIST=()
    UNSATISFIED_LIST=()

    for i in ${!PROJECT_LIST[@]} ; do

        PROJ_NAME=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 1)
        UNSATISFIED_LIST+=("${PROJ_NAME}")
        PROJ_VER=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 2)
        PROJ_DIR=$(echo ${PROJECT_LIST[i]} | cut -d ',' -f 3)

        cd $REPO_DIR
        cd $PROJ_NAME
        tempArray=( $(cat pkg/control/control | grep Depends | cut -d ':' -f 2 | tr ',' '\n' | cut -d '(' -f 1 | sed 's/ //g') )
        #echo "Project ${PROJ_NAME} Deps:"
        for i in ${!tempArray[@]} ; do
            depsMap["${PROJ_NAME}",$i]="${tempArray[i]}"
            #echo -e "\t${depsMap["${PROJ_NAME}",$i]}"
        done
        cd $HOME_DIR
    done

    __print_progress_bar 0 1

    local COUNTER=0
    while true ; do

        THIS_ROUND=()

        for PROJ_NAME in ${UNSATISFIED_LIST[@]} ; do

            REM_DEPS=()

            for i in {0..1000} ; do

                if [[ "${depsMap["${PROJ_NAME}",$i]}" == "" ]] ; then
                    break;
                fi

                # If
                if [[ " ${UNSATISFIED_LIST[*]} " =~ " ${depsMap["${PROJ_NAME}",$i]} " ]]; then
                    REM_DEPS+=("${depsMap["${PROJ_NAME}",$i]}")
                elif [[ $COUNTER == 0 ]] && ! [[ " ${KNOWN_IN_SYS_IMG[*]} " =~ " ${depsMap["${PROJ_NAME}",$i]} " ]]; then
                    MISSING_DEPS_LIST+=("${depsMap["${PROJ_NAME}",$i]}")
                fi

            done

            if [ ${#REM_DEPS[@]} -eq 0 ] ; then
                THIS_ROUND+=("${PROJ_NAME}")
            else
                for i in ${!REM_DEPS[@]} ; do
                    depsMap["${PROJ_NAME}",$i]="${REM_DEPS[i]}"
                done
            fi

        done

        if [ ${#THIS_ROUND[@]} -eq 0 ] ; then

            echo -e "${CLR_LIT_RED}${SET_BOLD}Conducted whole round without any additions, impossible situation reached${RESET_ALL}"
            echo "Remaining Projects:"
            echo ${UNSATISFIED_LIST[@]} | tr ' ' '\n' | sort | column

            __exit 1

        fi

        for i in ${!THIS_ROUND[@]} ; do

            # Add to the build list
            BUILD_LIST[$COUNTER,$i]="${THIS_ROUND[i]}"

            # And remove from the unsatisfied list
            for j in ${!UNSATISFIED_LIST[@]} ; do
                if [ " ${UNSATISFIED_LIST[j]} " ==  " ${THIS_ROUND[i]} " ] ; then
                    unset UNSATISFIED_LIST[j]
                    break
                fi
            done
        done

        UNSATISFIED_LIST=("${UNSATISFIED_LIST[@]}")

        if [ ${#UNSATISFIED_LIST[@]} -eq 0 ] ; then
            # no more unsatisfied Dependencies
            break
        fi

        COUNTER=$((COUNTER+1))

        __print_progress_bar $(( ${#PROJECT_LIST[@]} - ${#UNSATISFIED_LIST[@]} )) ${#PROJECT_LIST[@]}

    done

    if ! [ ${#MISSING_DEPS_LIST[@]} -eq 0 ] ; then
        echo
        echo
        echo -e "${CLR_LIT_RED}${SET_BOLD}The following packages were depended upon but are not in the list.${RESET_ALL}"
        echo -e "${CLR_LIT_RED}${SET_BOLD}\tEither fix the broken control files or add these ${RESET_ALL}"
        echo -e "${CLR_LIT_RED}${SET_BOLD}\tto the KNOWN_IN_SYS_IMG list before continuing${RESET_ALL}"
        echo ${MISSING_DEPS_LIST[@]} | tr ' ' '\n' | sort | column

        __exit 1

    fi

    NUM_BUILD_ROUNDS=$COUNTER

    __print_progress_bar 1 1

    echo
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Succcess----${RESET_ALL}"
}

__do_tagging() {

    local line_counts=()
    local i
    local j

    __scrollable_clear_terminal

    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Dependency Structure----${RESET_ALL}"

    # Print the initial list
    for (( i=0; i<=${NUM_BUILD_ROUNDS}; i++ )); do

        local THIS_ROUND=()
        local PRINT_LIST=()
        line_counts+=($(cursor-row))

        for j in {0..1000} ; do

            if [[ "${BUILD_LIST[$i,$j]}" == "" ]] ; then
                break;
            fi

            THIS_ROUND+=("${BUILD_LIST[$i,$j]}")
            PRINT_LIST+=("${CLR_WHT}${BUILD_LIST[$i,$j]}${RESET_ALL}")

        done

        echo -e "${CLR_LIT_YLW}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"
        echo -e ${PRINT_LIST[@]} | tr ' ' '\n' | column
        __print_progress_bar 0 1
        echo
        echo

    done

    echo -e "${SET_BOLD}Key:${RESET_ALL}"
    echo -e "${CLR_LIT_RED}Pipeline Failed\n${CLR_LIT_YLW}Pipeline Running\n${CLR_LIT_BLU}Pipeline Done\n${CLR_LIT_GRN}Success, In Apt Repo${RESET_ALL}"
    echo
    echo -e "${SET_BOLD}Release Name:${RESET_ALL} ${RELEASE_NAME}"
    echo

    read -p "Proceed with this setup(y/n)? "
    echo
    if ! [[ $REPLY =~ ^[Yy]$ ]] ; then
        __exit 0
    fi

    ## don't do this anymore
    ##__configure_gcloud_repo >/dev/null 2>/dev/null

    EXIT_LOCATION_X=$(cursor-col)
    EXIT_LOCATION_Y=$(cursor-row)

    __gotoxy 0 0
    echo -e "${CLEAR_LINE}${CLR_LIT_GRN}${SET_BOLD}----Tagging and waiting for CI----${RESET_ALL}"

    __gotoxy 0 ${line_counts[0]}

    for (( i=0; i<=${NUM_BUILD_ROUNDS}; i++ )); do

        ! $MAIN_RUNNING && break

        # State machine
        # 0  = unknown
        # 1  = failed
        # 2  = running
        # 3  = pipeline passed
        # 4  = pipeline passed and indexed on package repo
        declare -A PROJ_STATUS
        local THIS_ROUND=()
        local PRINT_LIST=()
        local num_ready=0

        for j in {0..1000} ; do

            if [[ "${BUILD_LIST[$i,$j]}" == "" ]] ; then
                break;
            fi

            THIS_ROUND+=("${BUILD_LIST[$i,$j]}")
            PRINT_LIST+=("${CLR_WHT}${BUILD_LIST[$i,$j]}${RESET_ALL}")
            PROJ_STATUS["${BUILD_LIST[$i,$j]}"]="0"

            # Private projects will not show their pipeline status so we'll have to assume they succeed
            for k in ${!PRIVATE_PROJECTS[@]} ; do
                if [[ " ${PRIVATE_PROJECTS[k]} " ==  " ${BUILD_LIST[$i,$j]} " ]] ; then
                    PROJ_STATUS["${BUILD_LIST[$i,$j]}"]="3"
                    PRINT_LIST[$j]="${CLR_LIT_BLU}${PROJ_NAME}${RESET_ALL}"
                    break
                fi
            done

            cd "${REPO_DIR}/${BUILD_LIST[$i,$j]}"
            if ! git show-ref | grep -q "${RELEASE_NAME}" ; then
                git tag ${RELEASE_NAME} >/dev/null 2>/dev/null
                git push --tags >/dev/null 2>/dev/null
            fi

        done

        cd ${HOME_DIR}

        __gotoxy 0 ${line_counts[i]}
        echo -e "${SET_BLINK}${CLR_LIT_YLW}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"
        echo -e ${PRINT_LIST[@]} | tr ' ' '\n' | column
        __print_progress_bar $num_ready ${#THIS_ROUND[@]}
        echo

        while $MAIN_RUNNING ; do

            local j
            for j in ${!THIS_ROUND[@]} ; do

                cd $HOME_DIR
                ! $MAIN_RUNNING && break

                PROJ_NAME=${THIS_ROUND[$j]}
                initial_status="${PROJ_STATUS[$PROJ_NAME]}"

                # These ones are already done
                [[ "$initial_status" == "4" ]] && continue

                if ! [[ "$initial_status" == "3" ]] ; then
                    PROJ_STATUS[$PROJ_NAME]="$( __check_pipeline_status $PROJ_NAME )"
                    case ${PROJ_STATUS[$PROJ_NAME]} in
                        "1")
                            PRINT_LIST[$j]="${CLR_LIT_RED}${PROJ_NAME}${RESET_ALL}"
                            ;;
                        "2")
                            PRINT_LIST[$j]="${CLR_LIT_YLW}${PROJ_NAME}${RESET_ALL}"
                            ;;
                        "3")
                            PRINT_LIST[$j]="${CLR_LIT_BLU}${PROJ_NAME}${RESET_ALL}"
                            ;;
                        *)
                            PRINT_LIST[$j]="${CLR_WHT}${PROJ_NAME}${RESET_ALL}"
                            ;;
                    esac
                fi

                ! $MAIN_RUNNING && break

                if [[ "${PROJ_STATUS[$PROJ_NAME]}" == "3" ]] && __is_package_ready $PROJ_NAME ; then
                    PRINT_LIST[$j]="${CLR_LIT_GRN}${PROJ_NAME}${RESET_ALL}"
                    PROJ_STATUS[$PROJ_NAME]="4"
                    num_ready=$(( $num_ready + 1 ))
                fi

                cd ${HOME_DIR}

                ! $MAIN_RUNNING && break

                # Reprint the list if something changed
                if ! [[ "${PROJ_STATUS[$PROJ_NAME]}" == "$initial_status" ]] ; then
                    __gotoxy 0 ${line_counts[i]}
                    echo -e "${SET_BLINK}${CLR_LIT_YLW}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"
                    echo -e ${PRINT_LIST[@]} | tr ' ' '\n' | column
                    __print_progress_bar $num_ready ${#THIS_ROUND[@]}
                    echo
                fi

            done

            if ! $MAIN_RUNNING ; then
                __gotoxy 0 ${line_counts[i]}
                echo -e "${CLR_LIT_RED}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"

                __gotoxy ${EXIT_LOCATION_X} ${EXIT_LOCATION_Y}

                __exit 1
            fi

            if [ $num_ready -eq ${#THIS_ROUND[@]} ] ; then
                __gotoxy 0 ${line_counts[i]}
                echo -e "${CLR_LIT_GRN}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"
                echo -e ${PRINT_LIST[@]} | tr ' ' '\n' | column
                __print_progress_bar 1 1
                echo
                break
            fi

            sleep 5

            if ! $MAIN_RUNNING ; then
                __gotoxy 0 ${line_counts[i]}
                echo -e "${CLR_LIT_RED}${SET_BOLD}----Round $(( ${i} + 1 ))----${RESET_ALL}"

                __gotoxy ${EXIT_LOCATION_X} ${EXIT_LOCATION_Y}

                __exit 1
            fi

        done
    done

    __gotoxy ${EXIT_LOCATION_X} ${EXIT_LOCATION_Y}
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Succcess----${RESET_ALL}"
}

__release_sdk_main(){

    __parse_opts "$@"

    if ! [ -f ${FILE_NAME} ]; then
        echo "ERROR: Missing ${FILE_NAME}, make sure this file is available and has the latest CSV from the spreadsheet"
        exit 1
    fi

    __scrollable_clear_terminal

    mkdir -p $REPO_DIR
    mkdir -p $TMP_DIR

    PROJECT_LIST=( $( cat ${FILE_NAME} | tr ';' '\n'  | sort ) )


    ## not used anymore, we want to safety check, tag, and monitor EVERYTHING
    ##__remove_flagged_projects

    __clone_projects

    if $PULL_PROJECTS ; then
        __pull_projects
    fi

    if [[ "$MODE" == "LIST_BRANCHES" ]]; then
        __list_extra_branches
        __exit 0
    fi

    __perform_safety_checks

    if [[ "$MODE" == "TEST" ]]; then
        __exit 0
    fi

    __prompt_release_name

    __build_dependency_structure

    __do_tagging

    __exit 0
}

__release_sdk_main "$@"

