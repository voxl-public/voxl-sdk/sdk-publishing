#!/bin/bash

set -e

RESET_ALL="\033[0m"
SET_BOLD="\033[1m"
CLR_LIT_GRN="\033[92m"

# we aren't building VOXL1 system images anymore, just keep using the last stable one
SYS_FILE="gs://system-image-releases/voxl/apq8096-system-4.0.0.tar.gz"
PLAT_NAME="voxl"

print_usage () {
    echo ""
    echo "Makes a platform release"
    echo ""
    echo "arguments are as follows:"
    echo ""
    echo "    -h | --help    - show this help text"
    echo "    -n | --nightly - build a nightly (skips all prompts)"
    echo "    -w | --weekly  - build a weekly (skips all prompts)"
    echo ""
    echo ""
}

parse_opts(){

    while [[ $# -gt 0 ]]; do
        ## parse arguments
        case ${1} in
            "h"|"-h"|"help"|"--help")
                print_usage
                exit 0
                ;;

            "-n"|"--nightly"|"nightly")
                REL_TYPE="nightly"
                ;;

            "-w"|"--weekly"|"weekly")
                REL_TYPE="weekly"
                ;;

            *)
                ## all other arguments are invalid
                echo "invalid option: $arg"
                print_usage
                exit -1
        esac
        shift
    done
}

clean_suite() {
    DIRECTORY="voxl-suite/"
    echo "Deleting all but the newest version of each ipk package"

    ## make a list of package names
    NAMES=( $( find ${DIRECTORY} | sed "s&${DIRECTORY}&&g" | grep -P -o "^.+?(?=_[0-9])" | sort | uniq | tr -d '/' | sed '/^$/d' ) )
    LEN=${#NAMES[@]}
    #__print_progress_bar $COUNT $LEN

    # echo ${NAMES[@]} | tr ' ' '\n'

    for i in ${NAMES[@]}; do

        PKGVERS=( $( ls ${DIRECTORY}${i}_* | sed "s&${DIRECTORY}${i}_&&g" | sed "s&.ipk&&g" | sort -r -V ) )

        echo
        echo "$i all versions:"
        SUBLEN=${#PKGVERS[@]}
        echo
        echo "keeping: ${PKGVERS[0]}"
        echo

        if [ $SUBLEN -gt 1 ]; then
        for j in $( seq 1 $((SUBLEN-1)) ); do
            ver=${PKGVERS[j]}
            rm -f ${DIRECTORY}${i}_${ver}.ipk
        done
        fi

    done

    echo ""
}

# needs to be run in the directoy with the script, has dependencies
if echo $0 | grep -q '/' ; then
    # echo "moving to: $(dirname $0)"
    cd $(dirname $0)
fi

parse_opts $@








if [[ "$REL_TYPE" == "nightly" ]]; then
    echo -e "${GRN}${SET_BOLD}----Starting nightly configuration----${RESET_ALL}"
    date="$(date +%Y%m%d | tr -d '\n' )"
    folder="${PLAT_NAME}_SDK_nightly_${date}"
    NEW_RELEASE="${folder}.tar.gz"

    SDK_VER="dev"



elif [[ "$REL_TYPE" == "weekly" ]]; then

    echo -e "${GRN}${SET_BOLD}----Starting weekly configuration----${RESET_ALL}"
    date="$(date +%Y%m%d | tr -d '\n' )"
    folder="${PLAT_NAME}_SDK_weekly_${date}"
    NEW_RELEASE="${folder}.tar.gz"

    SDK_VER="staging"

    echo "Chose system image: $SYS_FILE"
    echo -e "${GRN}${SET_BOLD}----Done configuring nightly setup----${RESET_ALL}"


#### not a nightly or weekly, run the wizard
else

    echo "Should be pull the system image from the system-image-releases bucket"
    echo "or the system-image-candidates bucket?"
    select REL_TYPE in release candidate
    do
        echo -n
        break
    done
    echo


    echo "Enter the SDK version corresponding to the debian package repo you"
    echo "wish to pull from (should follow the structure \"X.Y\")"
    echo "this could also be dev or staging"
    echo -en "${SET_BOLD}Version: ${RESET_ALL}"

    read SDK_VER
    echo

    # prepend sdk- to the version if it's only a version number
    if echo ${SDK_VER} | egrep -xq "[0-9]+(.[0-9]+)+" ; then
        SDK_VER="sdk-${SDK_VER}"
    fi

    echo "What is the final version string for this release?"
    echo "examples:"
    echo "1.0.0"
    echo "1.0.1"
    echo "1.0.0-beta3"

    read SDK_NAME
    echo

    folder="${PLAT_NAME}_SDK_${SDK_NAME}"
    NEW_RELEASE="${folder}.tar.gz"


    echo -e ""
    echo -e ""
    echo -e "${SET_BOLD}${GRN}Alright! here is the configuration we are going to use.${RESET_ALL}"
    echo -e ""
    echo -e "voxl-suite packages will be pulled from:"
    echo -e "${SET_BOLD}http://voxl-packages.modalai.com/dists/qrb5165/${SDK_VER}/binary-arm64/${RESET_ALL}"
    echo -e ""
    echo -e "system image version will be pulled from bucket:"
    echo -e "${SET_BOLD}$SYS_FILE${RESET_ALL}"
    echo -e ""
    echo -e "the folder the user will extract with everything in it will be called:"
    echo -e "${SET_BOLD}$folder${RESET_ALL}"
    echo -e ""
    echo -e "and the final tarball will be called:"
    echo -e "${SET_BOLD}$NEW_RELEASE${RESET_ALL}"
    echo -e ""
    echo -e ""
    echo -e "if this looks okay, please hit enter to start the build!"
    read
    echo ""
fi











rm -rf "$folder"
rm -rf "$folder.tar.gz"
mkdir  "$folder"
cd     "$folder"
cp ../scripts/install.sh .

################################################################################
echo -e "${CLR_LIT_GRN}${SET_BOLD}----Getting System Image----${RESET_ALL}"
mkdir system-image
cd system-image

### TODO It's possible that this fails to grab the latest release candidate if we get past 9 i.e.
gsutil cp "${SYS_FILE}" .

NUM_FILES=$(ls | wc -l)

if [[ "$NUM_FILES" == "0" ]] ; then
    echo "FAILED: did not find a valid system image"
    exit -1
fi

TAR_NAME="$(ls)"
tar -xzvf "${TAR_NAME}"
rm "${TAR_NAME}"

[ -f flash_build_apps.py ] && rm flash_build_apps.py
cp ../../scripts/flash-system-image.sh .

echo
cd ..

################################################################################
echo -e "${CLR_LIT_GRN}${SET_BOLD}----Getting SDK----${RESET_ALL}"
mkdir voxl-suite
cd voxl-suite

cp ../../scripts/offline-install.sh .
sed -i "s/DUMMY-SDK-REPO-TARGET/${SDK_VER}/g" offline-install.sh

wget -r -nv -np -nH -R "index.html" -R "index.html.tmp" -R "Packages" -R "Packages.stamps" --cut-dirs=4 \
    "http://voxl-packages.modalai.com/dists/apq8096/${SDK_VER}/binary-arm64/"

echo
cd ..

clean_suite

cd ..

################################################################################
if [ -d pre-tweaks ] ; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Found existing pre-tweaks, adding----${RESET_ALL}"
    cp -r pre-tweaks "${folder}/"
    echo
fi

if [ -d post-tweaks ] ; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Found existing post-tweaks, adding----${RESET_ALL}"
    cp -r post-tweaks "${folder}/"
    echo
fi
################################################################################


# if [ ! "$REL_TYPE" = "nightly" ] && [ ! "$REL_TYPE" = "weekly" ]; then
#     echo -e "${CLR_LIT_GRN}${SET_BOLD}----Done----${RESET_ALL}"
#     echo
#     echo "System image and Suite are ready, validate tweaks now if applicable"
#     echo -en "${SET_BOLD}Press enter when ready to continue${RESET_ALL}"
#     read

#     echo
# fi




################################################################################
## fetch flight core v1 and v2 binaries
## TODO handle picking a stable one, right now just the latest
## also TODO, some more error checking and maybe add a readme for flight core
################################################################################

echo -e "${CLR_LIT_GRN}${SET_BOLD}----Getting Flight Core Binaries----${RESET_ALL}"
mkdir -p ${folder}/flight-core
cd ${folder}/flight-core

if [ "$REL_TYPE" == "nightly" ]; then
    FC_URL1="http://voxl-packages.modalai.com/dists/fc-v1/dev/"
    FC_URL2="http://voxl-packages.modalai.com/dists/fc-v2/dev/"
elif [ "$REL_TYPE" == "weekly" ]; then
    FC_URL1="http://voxl-packages.modalai.com/dists/fc-v1/staging/"
    FC_URL2="http://voxl-packages.modalai.com/dists/fc-v2/staging/"
else
    FC_URL1="http://voxl-packages.modalai.com/dists/fc-v1/$SDK_VER/"
    FC_URL2="http://voxl-packages.modalai.com/dists/fc-v2/$SDK_VER/"
fi

wget $FC_URL1
FCV1_BIN=$(grep -o '"modalai_fc[^[:space:]]*\.px4"' index.html | tail -n 1)
FCV1_BIN="${FCV1_BIN#\"}"
FCV1_BIN="${FCV1_BIN%\"}"
if [ "$FCV1_BIN" == "" ]; then
    echo "ERROR, failed to parse fcv1 bins"
    exit 1
fi
echo "chose $FCV1_BIN"

rm -f index.html
wget $FC_URL2
FCV2_BIN=$(grep -o '"modalai_fc[^[:space:]]*\.px4"' index.html | tail -n 1)
FCV2_BIN="${FCV2_BIN#\"}"
FCV2_BIN="${FCV2_BIN%\"}"
if [ "$FCV2_BIN" == "" ]; then
    echo "ERROR, failed to parse fcv2 bins"
    exit 1
fi
echo "chose $FCV2_BIN"

rm -f index.html
wget ${FC_URL1}${FCV1_BIN}
wget ${FC_URL2}${FCV2_BIN}

cd ../../



################################################################################
echo -e "${GRN}${SET_BOLD}----Tarring (be patient)----${RESET_ALL}"
tar -czvf "$NEW_RELEASE" "$folder"
echo ""
echo -e "${GRN}${SET_BOLD}----DONE creating $NEW_RELEASE----${RESET_ALL}"
echo ""

## all done building automatic builds for CI
if [ "$REL_TYPE" == "nightly" ] || [ "$REL_TYPE" == "weekly" ]; then
    exit 0
fi

################################################################################
BUCKET_NAME="gs://platform-releases/$PLAT_NAME/"
PUB_URL="https://storage.googleapis.com/platform-releases/${PLAT_NAME}/${NEW_RELEASE}"

echo "would you like to upload it to the following bucket?"
echo "$BUCKET_NAME"

select OPT in "yes" "no"; do
    case $OPT in
    "no")
        echo "quitting"
        exit 0
        ;;
    "yes")
        echo "uploading"
        gsutil cp "${NEW_RELEASE}" "${BUCKET_NAME}"
        echo "done uploading new release available at:"
        echo "${BUCKET_NAME}${NEW_RELEASE}"
        echo "$PUB_URL"

        break
        ;;
    *)
      echo "Invalid selection, try again"
    esac
done




exit 0
