# tools to making platform release for APQ8096

This repository contains tools to download and tar-up the suite packages and make a "platform release" including system image and installer.


## Step 1: updating the voxl-suite package details

 - Update the control file version (obviously).

 - Copy the auto-generated package dependency list from the ModalAI internal voxl-suite spreadsheet and paste into the control file.

 - If necessary, increase the system image dependency in the preinst script.


## Step 2: Make sure new voxl-suite is up in the public binary repo



## Step 3: Making a voxl-suite offline installer

The make_offline_installer.sh script will download the latest of every package in the voxl-packages stable repository, minus a few select deprecated packages that are no longer part of voxl-suite.

The produced offline installer is a tarball of the form voxl-suite_x.x.x.tar.gz that contains the voxl-suite ipk you just generated, all the latest stuff from the stable repo, and an offline installer script to install on a VOXL over adb.

### Options

Generally you will just run ./make_offline_installer.sh to make an offline installer from the current state of the stable repo. For development you can also make an installer from the dev repository by providing the `dev` argument.

There is an optional breakpoint in this script where you can remove and edit things manually if you wish. Enable this with the `break` argument.


## Step 4: Making a voxl-platform release tarball

A voxl-platform release is a tarball containing a voxl system image, the voxl-suite offline installer that you just build, and an install script to install both in order.

Download the voxl system image and place in the root directory of this repository. It should have the form modalai-x-x-x.tgz. This is a big file, so running ./clean.sh won't delete it so save you the trouble of getting it again.

Now run ./make_platform_release.sh and the script should unpack the system image and the voxl-suite offline installer you just made into the `working` directory and repack them together with the platform-release install.sh script for the customers and production team to use.

The platform release will be placed in the root of this repository with a name matching the form  voxl_platform_3-3-0-0.5.0-a.tar.gz

The `a` suffix indicates revisions in the platform release packing or installer which are not revisions in the system image or voxl-suite itself. For example improvements to the install script or added files would constitute a bump to a `b` suffix.


## Step 5: Test the platform release

To test the platform release, you don't need to extract the final tar.gz. Just run the install script in the working directory. That's what was used to build the tar.gz and won't be deleted until you run the make_platform_release.sh or clean.sh scripts.

```
cd working/voxl_platform*
./install.sh
```

