#!/bin/bash

################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

RESET_ALL="\033[0m"
SET_BOLD="\033[1m"
CLR_LIT_GRN="\033[92m"

MODE="default"

FILE=$(which adb)
if [ -f "$FILE" ]; then
	echo "[INFO] adb installed"
else
	echo ""
	echo "[Error]: android-tools-adb missing, use: apt-get install android-tools-adb"
	echo ""
	exit 1
fi

FILE=$(which fastboot)
if [ -f "$FILE" ]; then
	echo "[INFO] fastboot installed"
else
	echo ""
	echo "[Error]: android-tools-fastboot missing, use: apt-get install android-tools-fastboot"
	echo ""
	exit 1
fi

set -e

# verify folders
if ! [ -d system-image ]; then
	echo ""
	echo "[Error]: missing system-image directory"
	echo ""
	exit 1
fi

if ! [ -d voxl-suite ]; then
    echo ""
    echo "[Error]: missing voxl-suite directory"
    echo ""
    exit 1
fi

echo -e "${CLR_LIT_GRN}${SET_BOLD}----Starting System Image Flash----${RESET_ALL}"
cd system-image
./flash-system-image.sh

if [ "$?" -ne 0 ]; then
	echo '[ERROR] Exiting'
	exit 1
fi

cd ..

adb wait-for-device

if [ -d pre-tweaks ]; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Applying Pre Tweaks----${RESET_ALL}"
    cd pre-tweaks
    ./apply_tweaks.sh
    cd ..
fi

echo -e "${CLR_LIT_GRN}${SET_BOLD}----Installing Suite----${RESET_ALL}"
cd voxl-suite
./offline-install.sh
cd ..



## scroll down the screen and configure SKU which is going to start by calling `clear`
printf '\33[H\33[2J'
adb shell voxl-configure-sku --wizard

## scroll down the screen and configure MPA which is going to start by calling `clear`
printf '\33[H\33[2J'
adb shell voxl-configure-mpa --non-interactive




echo -e "${CLR_LIT_GRN}${SET_BOLD}----Installation Completed----${RESET_ALL}"

if [ -d post-tweaks ]; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Applying Post Tweaks----${RESET_ALL}"
    cd post-tweaks
    ./apply_tweaks.sh
    cd ..
fi

echo "rebooting device..."
adb reboot
adb wait-for-device && echo "Success"
