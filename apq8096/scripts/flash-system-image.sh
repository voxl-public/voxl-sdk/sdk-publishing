#!/bin/bash

################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

function flash {

	if [ "$(adb devices)" == "" ] && [ "$(fastboot devices)" == "" ]; then
		echo "[ERROR] Device is not in adb or in fastboot! Exiting."
		exit 1
	fi

	APPSBOOT=emmc_appsboot.mbn
	BOOT=apq8096-boot.img
	SYSTEM=apq8096-sysfs.ext4
	USER=apq8096-usrfs.ext4
	PERSIST=apq8096-persist.ext4
	CACHE=apq8096-cache.ext4
	RECOVERY=apq8096-recovery.ext4

	set -e
	test -e $APPSBOOT
	test -e $BOOT
	test -e $SYSTEM
	test -e $USER
	test -e $PERSIST
	test -e $CACHE
	test -e $RECOVERY

	echo "[INFO] Rebooting into fastboot..."

	# if already in fastboot mode, this command bails out
	set +e
	adb reboot bootloader
	set -e

	while [ "$(fastboot devices)" == "" ]
	do
		echo "[INFO] Waiting for fastboot..."
		sleep 2
	done

	fastboot flash aboot $APPSBOOT
	fastboot flash boot $BOOT
	fastboot flash system $SYSTEM
	if [ $ERASE_DATA_PARTITION == 1 ]; then
		fastboot flash userdata $USER
	fi
	fastboot flash persist $PERSIST
	fastboot flash cache $CACHE
	fastboot flash recoveryfs $RECOVERY

	echo "[INFO] Done flashing all images. Rebooting device in 2 seconds"
	sleep 2

	fastboot reboot
	echo "[INFO] Waiting for device..."
	adb wait-for-device
	adb shell version
	echo ""
	echo "[INFO] Finished!"
	echo ""
	echo "The next step is to follow the VOXL Quickstart guides"
	echo "to connect to a network and install supporting software."
	echo "https://docs.modalai.com/voxl-quickstarts/"
}

function wipe_data_prompt {

	echo ""
	echo "This process will completely wipe your VOXL's file system but will"
	echo "preserve the /data/ partition which contains things like calibration,"
	echo "wifi config, and docker images."
	echo "you can optionally wipe the /data/ parition, although we encourage"
	echo "you not to unless you have a good reason."
	echo ""
	select opt in "preserve(recommended)" "wipe"; do
		case $opt in
		"preserve(recommended)" )
			ERASE_DATA_PARTITION=0
			break;;
		"wipe" )
			ERASE_DATA_PARTITION=1
			break;;
		*)
			echo "invalid option"
			esac
	done
}


ERASE_DATA_PARTITION=0
if [ "$#" -eq 1 ]; then
	if [ "$1" == "-f" ]; then
		ERASE_DATA_PARTITION=1
		echo "[INFO] Using factory mode"
	fi
fi

# Only do with -f flag, no more user confusion
# if [ $ERASE_DATA_PARTITION == 0 ]; then
# 	wipe_data_prompt
# fi

flash
