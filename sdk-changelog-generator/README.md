# change_logs

This script can be used to compare between two versions of SDK's (eg: sdk-1.0/ and sdk-0.9/)

## 1. Prerequisites

- Python Modules
  - tkinter
  - beautiful-soup
  - gitlab
  - requests
  - Run 


To install:

```
sudo apt update
sudo apt install -y python3-tk
pip3 install -r requirements.txt
```


## 2. Set your Gitlab API Token (Optional)

If you a ModalAI developer, you can give this took extra access to read the changelogs of private repositories by entering a gitlab token.

If you don't do this step, you will be still able to see the version comparison, but the changelog will be missing entries for private repos.

Log into gitlab and generate a new token with read-repo priviledges here:

```
https://gitlab.com/-/profile/personal_access_tokens
```

Copy the token into a new file here in this directory:

```
echo {TOKEN} > gitlab_token.txt
````

<mark> WARNING: Please do not push the gitlab_token.txt file. It is in .gitignore to help prevent this</mark>


## 3. Launch

```
./main.py
OR
Python3 main.py
```

## 4. Use

Within the GUI is a help menu with further instructions.
