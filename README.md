# VOXL SDK Publishing Tools 🚀

## Overview 📜 
This repository contains CI/CD configurations for nightly and weekly builds of multiple platforms, including:
- `voxl2_mini_nightly`
- `voxl2_nightly`
- `voxl_nightly`
- `ubun2.0-multi_nightly`

## Build Status 🛠️
| Job Name | Status |
|----------|--------|
| **VOXL2 Mini Nightly** | ![VOXL2 Mini](https://gitlab.com/voxl-public/voxl-sdk/publishing-tools/badges/master/pipeline.svg?job=voxl2_mini_nightly) |
| **VOXL2 Nightly** | ![VOXL2](https://gitlab.com/voxl-public/voxl-sdk/publishing-tools/badges/master/pipeline.svg?job=voxl2_nightly) |
| **APQ8096 Nightly** | ![APQ8096](https://gitlab.com/voxl-public/voxl-sdk/publishing-tools/badges/master/pipeline.svg?job=voxl_nightly) |
| **Ubuntu 2.0 Multi Nightly** | ![Ubuntu Multi](https://gitlab.com/voxl-public/voxl-sdk/publishing-tools/badges/master/pipeline.svg?job=ubun2.0-multi_nightly) |


Tools for making platform releases for apq8096 and qrb5165 systems
for ModalAI internal and CI use only

### Dependencies

 - Internet access

 - Configured gsutil to the modalai system image bucket

### Manual Instructions 

cd into the directory

```
cd {apq8096/qrb5165}
```

run the script:

```
./make-platform-release.sh
```

The script will ask a couple questions:

 - Which hardware type (QRB5 only)

 - Pull release or candidate system image

 - System image version

 - SDK Version


Then pull all the required files for the system image and SDK, wait to allow
placing a temp-tweaks directory if necessary, and  tar/gzip the whole platform.


## SDK Tools 

### Release SDK

This is a powerful tool that requires maintainer status on the git repos and access
to the google cloud server hosting the package repos. The tool will clone all of the
projects specified in the projects.csv, perform a number of safety checks, and then
allow the user to tag all of the repos with an sdk tag, pushing the tags up in the
correct order so as to allow all packages to finish building before any that might
depend on them.

### Update CI Status Page

A simple tool that clones the CI status repo and updates the readme with the list
of packages from the projects.csv. Options for showing a specific tag's ci stages
and showing which platforms the packages are supported on

### Generate Change Table

A tool that compares two csv files listing projects and builds a markdown table
of a changelog between them, showing a list of packages and their: updated versions,
new packages, unchanged packages, and option to show which platforms the
packages are supported on.

### Get Pipeline Status

A helper python script for the release-sdk script getting the pipeline status from
their badge, shouldn't be run on it's own.
