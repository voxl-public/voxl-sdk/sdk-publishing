#!/bin/bash

set -e


WEEKLY_SYSTEM_IMAGE_VOXL2="gs://system-image-nightlies/voxl2/1.7.1-M0054-14.1a-perf-nightly-20231025.tar.gz"
WEEKLY_SYSTEM_IMAGE_VOXL2_MINI="gs://system-image-nightlies/voxl2-mini/1.7.1-M0104-14.1a-perf-nightly-20231025.tar.gz"
WEEKLY_SYSTEM_IMAGE_RB5="gs://system-image-nightlies/rb5/1.7.1-M0052-14.1a-perf-nightly-20231025.tar.gz"


RESET_ALL="\033[0m"
SET_BOLD="\033[1m"
GRN="\033[92m"

# Default SDK version
SDK_VERSION="sdk1.0"
PLAT_NAME=""

print_usage () {
    echo ""
    echo "Makes a platform release"
    echo ""
    echo "arguments are as follows:"
    echo ""
    echo "    -h | --help                - show this help text"
    echo "    -n | --nightly {Platform}  - build a nightly for the given platform (skips all prompts)"
    echo "    -w | --weekly {Platform}   - build a weekly for the given platform (skips all prompts)"
    echo "    -s | --sdk {sdk1.0|sdk2.0} - specify SDK version (defaults to sdk1.0)"
    echo ""
}

parse_opts(){

    while [[ $# -gt 0 ]]; do
        ## parse arguments
        case ${1} in
            "h"|"-h"|"help"|"--help")
                print_usage
                exit 0
                ;;

            "-n"|"--nightly"|"nightly")
                if [[ $# -eq 1 ]]; then
                    echo "Nightly mode requires a platform type"
                    echo "e.g. voxl2-mini voxl2 rb5"
                    exit -1
                fi
                REL_TYPE="nightly"
                PLAT_NAME="$2"
                shift
                ;;

            "-w"|"--weekly"|"weekly")
                if [[ $# -eq 1 ]]; then
                    echo "weekly mode requires a platform type"
                    echo "e.g. voxl2-mini voxl2 rb5"
                    exit -1
                fi
                REL_TYPE="weekly"
                PLAT_NAME="$2"
                shift
                ;;

            "-s"|"--sdk")
                if [[ $# -eq 1 ]]; then
                    echo "SDK option requires a value (sdk1.0 or sdk2.0)"
                    exit -1
                fi
                if [[ "$2" != "sdk1.0" && "$2" != "sdk2.0" ]]; then
                    echo "Invalid SDK version. Allowed values are sdk1.0 or sdk2.0."
                    exit -1
                fi
                SDK_VERSION="$2"
                shift
                ;;

            *)
                ## all other arguments are invalid
                echo "invalid option: $arg"
                print_usage
                exit -1
        esac
        shift
    done

    if [[ "$SDK_VERSION" == "sdk1.0" ]]; then
        if [[ $REL_TYPE == "nightly" ]] || [[ $REL_TYPE == "weekly" ]]; then
            if [ ! $PLAT_NAME == "voxl2" ] && [ ! $PLAT_NAME == "voxl2-mini" ] && [ ! $PLAT_NAME == "rb5" ]; then
                echo "ERROR, paltform name must be one of voxl2, voxl2-mini, or rb5"
                exit 1
            fi
        fi
    fi
}

set_pub_url() {
    if [[ "$SDK_VERSION" == "sdk1.0" ]]; then
        PUB_URL="https://storage.googleapis.com/platform-releases/${PLAT_NAME}/${NEW_RELEASE}"
        SDK_REPO="http://voxl-packages.modalai.com/dists/qrb5165/${SDK_VER}/binary-arm64/"
        BUCKET_NAME="gs://platform-releases/$PLAT_NAME/"
    else
        PUB_URL="https://storage.googleapis.com/platform-releases/sdk2.0/${NEW_RELEASE}"
        # TEMPORARY FIX:
        # As SDK2.0 development is underway, CI for these packages is slowly being brought up
        # In the meantime let's start generating internal platform releases using the dev packages.
        # 
        # Once more packages are supported in sdk2.0, we can revert to pulling from the sdk2.0 repo
        # SDK_REPO="http://voxl-packages.modalai.com/dists/qrb5165-2/sdk-2.0/binary-arm64/"
        SDK_REPO="http://voxl-packages.modalai.com/dists/qrb5165-2/dev/binary-arm64/"
        
        BUCKET_NAME="gs://platform-releases/sdk2.0"
    fi
}


clean_suite() {
    DIRECTORY="voxl-suite/"
    echo "Deleting all but the newest version of each deb package"

    ## make a list of package names
    NAMES=( $( find ${DIRECTORY} | sed "s&${DIRECTORY}&&g" | grep -P -o "^.+?(?=_[0-9])" | sort | uniq | tr -d '/' | sed '/^$/d' ) )
    LEN=${#NAMES[@]}

    echo "all packages:"
    echo ${NAMES[@]} | tr ' ' '\n'

    for i in ${NAMES[@]}; do

        ## all files ending in _arm64.deb and sorted by version number
        PKGVERS=( $( ls ${DIRECTORY}${i}_* | sed "s&${DIRECTORY}${i}_&&g" | sed "s&_arm64.deb&&g" | sort -r -V ) )

        echo
        echo "$i all versions:"
        SUBLEN=${#PKGVERS[@]}
        echo
        echo "keeping: ${PKGVERS[0]}"
        echo


        if [ $SUBLEN -gt 1 ]; then
        for j in $( seq 1 $((SUBLEN-1)) ); do
            ver=${PKGVERS[j]}
            rm -f ${DIRECTORY}${i}_${ver}_arm64.deb
        done
        fi

    done

    echo ""
}

# needs to be run in the directoy with the script, has dependencies
if echo $0 | grep -q '/' ; then
    # echo "Moving to: $(dirname $0)"
    cd $(dirname $0)
fi

parse_opts $@

if [[ "$REL_TYPE" == "nightly" ]]; then
    echo -e "${GRN}${SET_BOLD}----Starting nightly configuration----${RESET_ALL}"
    date="$(date +%Y%m%d | tr -d '\n' )"
    folder="${PLAT_NAME}_SDK_nightly_${date}"
    NEW_RELEASE="${folder}.tar.gz"

    SDK_VER="dev"

    echo -e "${GRN}${SET_BOLD}----Finding System Image----${RESET_ALL}"
    SYS_FILE="$( gsutil ls "gs://system-image-nightlies/${PLAT_NAME}/" | tail -n 1 )"
    echo "Chose system image: $SYS_FILE"
    echo -e "${GRN}${SET_BOLD}----Done configuring nightly setup----${RESET_ALL}"


elif [[ "$REL_TYPE" == "weekly" ]]; then

    echo -e "${GRN}${SET_BOLD}----Starting weekly configuration----${RESET_ALL}"
    date="$(date +%Y%m%d | tr -d '\n' )"
    folder="${PLAT_NAME}_SDK_weekly_${date}"
    NEW_RELEASE="${folder}.tar.gz"

    SDK_VER="staging"

    if [[ "$PLAT_NAME" == "voxl2-mini" ]]; then
        echo "voxl2-mini release detected."
        SYS_FILE=$WEEKLY_SYSTEM_IMAGE_VOXL2_MINI
    elif [[ "$PLAT_NAME" == "rb5" ]]; then
        echo "rb5 release detected."
         SYS_FILE=$WEEKLY_SYSTEM_IMAGE_RB5
    else
        echo "voxl2 release detected."
         SYS_FILE=$WEEKLY_SYSTEM_IMAGE_VOXL2
    fi

    echo "Chose system image: $SYS_FILE"
    echo -e "${GRN}${SET_BOLD}----Done configuring weekly setup----${RESET_ALL}"


#### not a nightly or weekly, run the wizard
else
    echo "What platform is this release for?"
    select PLAT_NAME in voxl2 voxl2-mini rb5
    do
        echo "Selected platform: $PLAT_NAME"
        break
    done
    echo

    echo "Should be pull the system image from the system-image-releases bucket"
    echo "or the system-image-candidates bucket?"
    select REL_TYPE in release candidate
    do
        echo -n
        break
    done
    echo

    echo "Enter the system image version to pull (should follow the structure \"X.Y.Z\")"
    echo "Options are:"
    gsutil ls "gs://system-image-${REL_TYPE}s/${PLAT_NAME}/"
    echo ""
    echo -en "${SET_BOLD}Version: ${RESET_ALL}"

    read SYS_VER
    echo

    echo "Enter the SDK version corresponding to the debian package repo you"
    echo "wish to pull from (should follow the structure \"X.Y\")"
    echo "this could also be dev or staging"
    echo -en "${SET_BOLD}Version: ${RESET_ALL}"

    read SDK_VER
    echo

    # prepend sdk- to the version if it's only a version number
    if echo ${SDK_VER} | egrep -xq "[0-9]+(.[0-9]+)+" ; then
        SDK_VER="sdk-${SDK_VER}"
    fi

    echo "What is the final version string for this release?"
    echo "examples:"
    echo "1.0.0"
    echo "1.0.1"
    echo "1.0.0-beta3"

    read SDK_NAME
    echo

    folder="${PLAT_NAME}_SDK_${SDK_NAME}"
    NEW_RELEASE="${folder}.tar.gz"

    ### TODO It's possible that this fails to grab the latest release candidate if we get past 9 i.e. 10 is sorted lower than 9
    echo "querying gs://system-image-${REL_TYPE}s/${PLAT_NAME}/ for the requested system image"
    echo "This might take a minute while gsutil logs in"
    SYS_FILE="$( gsutil ls "gs://system-image-${REL_TYPE}s/${PLAT_NAME}/" | grep "${SYS_VER}" | tail -n 1 )"

    echo -e ""
    echo -e ""
    echo -e "${SET_BOLD}${GRN}Alright! here is the configuration we are going to use.${RESET_ALL}"
    echo -e ""
    echo -e "voxl-suite packages will be pulled from:"
    echo -e "${SET_BOLD}${SDK_REPO}${RESET_ALL}"
    echo -e ""
    echo -e "system image version will be pulled from bucket:"
    echo -e "${SET_BOLD}$SYS_FILE${RESET_ALL}"
    echo -e ""
    echo -e "the folder the user will extract with everything in it will be called:"
    echo -e "${SET_BOLD}$folder${RESET_ALL}"
    echo -e ""
    echo -e "and the final tarball will be called:"
    echo -e "${SET_BOLD}$NEW_RELEASE${RESET_ALL}"
    echo -e ""
    echo -e ""
    echo -e "if this looks okay, please hit enter to start the build!"
    read
    echo ""
fi

set_pub_url

echo "Using SDK version: $SDK_VERSION"
echo "PUB_URL: $PUB_URL"
echo "SDK_REPO: $SDK_REPO"

echo -e "${GRN}${SET_BOLD}----Setting up folder structure----${RESET_ALL}"
rm -rf "$folder"
rm -rf "$folder.tar.gz"
mkdir  "$folder"
cd     "$folder"
cp ../scripts/install.sh .
cp ../scripts/README.md .

################################################################################
echo -e "${GRN}${SET_BOLD}----Getting System Image----${RESET_ALL}"
mkdir system-image
cd system-image


echo "downloading ${SYS_FILE}"
gsutil cp "${SYS_FILE}" .

NUM_FILES=$(ls | wc -l)

if [[ "$NUM_FILES" == "0" ]] ; then
    echo "FAILED: did not find a valid system image"
    exit -1
fi

TAR_NAME="$(ls)"
tar -xzvf "${TAR_NAME}"
rm "${TAR_NAME}"
FOLDER_NAME="$(ls)"
mv ${FOLDER_NAME}/* .
rmdir "${FOLDER_NAME}"

echo
cd ..

################################################################################
echo -e "${GRN}${SET_BOLD}----Getting SDK----${RESET_ALL}"
mkdir voxl-suite
cd voxl-suite

cp ../../scripts/offline-install.sh .
echo "$SDK_VER" > REPO_SOURCE.txt

wget -r -nv -np -nH -R "index.html" -R "index.html.tmp" -R "Packages" -R "Packages.gz" --cut-dirs=4 \
    ${SDK_REPO}

echo
cd ..

clean_suite

cd ..

################################################################################
if [ -d pre-tweaks ] ; then
    echo -e "${GRN}${SET_BOLD}----Found existing pre-tweaks, adding----${RESET_ALL}"
    cp -r pre-tweaks "${folder}/"
    echo
fi

if [ -d post-tweaks ] ; then
    echo -e "${GRN}${SET_BOLD}----Found existing post-tweaks, adding----${RESET_ALL}"
    cp -r post-tweaks "${folder}/"
    echo
fi

# if [ ! "$REL_TYPE" = "nightly" ] && [ ! "$REL_TYPE" = "weekly" ]; then
# ################################################################################
#     echo -e "${GRN}${SET_BOLD}----Done----${RESET_ALL}"
#     echo
#     echo "System image and Suite are ready, validate tweaks now if applicable"
#     echo -en "${SET_BOLD}Press enter when ready to continue${RESET_ALL}"
#     read

#     echo
# fi

################################################################################
echo -e "${GRN}${SET_BOLD}----Tarring (be patient)----${RESET_ALL}"
tar -czvf "$NEW_RELEASE" "$folder"
echo ""
echo -e "${GRN}${SET_BOLD}----DONE creating $NEW_RELEASE----${RESET_ALL}"
echo ""

## all done building automatic builds for CI
if [ "$REL_TYPE" == "nightly" ] || [ "$REL_TYPE" == "weekly" ]; then
    exit 0
fi


################################################################################

echo "would you like to upload it to the following bucket?"
echo "$BUCKET_NAME"

select OPT in "yes" "no"; do
    case $OPT in
    "no")
        echo "quitting"
        exit 0
        ;;
    "yes")
        echo "uploading"
        gsutil cp "${NEW_RELEASE}" "${BUCKET_NAME}"
        echo "done uploading new release available at:"
        echo "${BUCKET_NAME}${NEW_RELEASE}"
        echo "$PUB_URL"

        break
        ;;
    *)
      echo "Invalid selection, try again"
    esac
done




exit 0
