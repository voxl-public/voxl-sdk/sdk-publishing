#!/bin/bash

PKG_DIR=/data/voxl-suite-offline-packages

CLEAR_LINE="\033[2K"

__print_progress_bar(){

    if [ $# -eq 3 ] ; then
        NUM_CHARS=$3
    else
        # 10 is number of characters in "](xxx%) " at the end of the line
        # plus one "[" at the beginning and one extra at the end so that
        # ctrl+c doesn't run over
        NUM_CHARS=$((COLUMNS - 10))
    fi

    ((PCT=$1*100/$2))
    ((VAL=$PCT*$NUM_CHARS/100))

    local STRING="\r["

    local i
    for i in $(seq 1 $NUM_CHARS); do

        if [ $VAL -ge $i ]; then
            STRING="${STRING}-"
        else
            STRING="${STRING} "
        fi
    done
    STRING="${STRING}]($(printf "%3d" $PCT)%)"
    echo -ne "$STRING"
}

cursor-row(){
    local COL
    local ROW
    IFS=';' read -sdR -p $'\E[6n' ROW COL
    echo "${ROW#*[}"
}

__gotoxy(){

    echo -ne "\033[$2;$1H"
}

# Sanity check adb is installed
FILE=$(which adb)
if [ -f $FILE ]; then
    echo "[INFO] adb installed"
else
    echo ""
    echo "[Error]: android-tools-adb missing, use: apt-get install android-tools-adb"
    echo ""
    exit 1
fi


# Connect and create directory to push IPKs to on target
echo "[INFO] Waiting for device"
adb wait-for-device

# push files over
echo "[INFO] Pushing packages to target"
adb shell rm -rf $PKG_DIR
adb shell mkdir -p $PKG_DIR

ROW="$(cursor-row)"
NUM=$( ls -l *.deb | wc -l )
i=0
echo

for FILE in *.deb; do

    __print_progress_bar ${i} $NUM 64
    echo

    __gotoxy 0 $ROW

    echo -en "${CLEAR_LINE}"
    adb push $FILE $PKG_DIR/
    let i++
done

__gotoxy 0 $ROW
echo -e "${CLEAR_LINE}Done Pushing"
__print_progress_bar 1 1 64
echo

# make both types of packages file, apt usually uses the .gz but installing
# docker fails if there is no uncompressed file, so do both
adb shell "cd $PKG_DIR/ && dpkg-scanpackages . /dev/null | gzip -9c > Packages.gz"
adb shell "cd $PKG_DIR/ && dpkg-scanpackages . /dev/null > Packages"

adb shell "echo \"deb [trusted=yes] file:$PKG_DIR ./\" > /etc/apt/sources.list.d/local.list"
adb shell "apt-get update -o Dir::Etc::sourcelist=\"/etc/apt/sources.list.d/local.list\" -o Dir::Etc::sourceparts=\"-\" -o APT::Get::List-Cleanup=\"0\""

while adb shell "fuser /var/lib/dpkg/lock-frontend" | grep -q lock ; do
    sleep 1
done

echo "[INFO] Installing packages on target"
adb shell "rm -f /tmp/success && apt install -y voxl-suite && echo true > /tmp/success"
SUCCESS=$(adb shell cat /tmp/success)

if [[ "$SUCCESS" == "true"* ]]; then
    echo "[INFO] Successfully install voxl-suite from offline packages"
else
    echo "ERROR failed to install voxl-suite"
    echo SUCCESS:$SUCCESS
    exit 1
fi


## see is make-platform-release.sh put a REPO_SOURCE.txt file in here to
## tell us what repo the voxl should use after offline install. If not
## assume staging
NEW_REPO="staging"
if [ -f "REPO_SOURCE.txt" ]; then
    NEW_REPO=$(cat REPO_SOURCE.txt)
    adb push REPO_SOURCE.txt $PKG_DIR/
fi

echo "[INFO] Pointing apt sources to $NEW_REPO repo"
adb shell "rm -f /tmp/success && voxl-configure-pkg-manager $NEW_REPO && echo true > /tmp/success"
SUCCESS=$(adb shell cat /tmp/success)

if [[ ! "$SUCCESS" == "true"* ]]; then
    echo "ERROR failed to configure apt sources"
    exit 1
fi

echo "[INFO] Done installing voxl-suite in offline mode"
exit 0
