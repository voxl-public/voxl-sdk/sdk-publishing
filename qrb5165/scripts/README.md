## Summary

### What is it?

A VOXL SDK release consists of a system image and the voxl-suite collection of packages that run on VOXL.

### Do I need to install it?

`VOXL` boards ship with the latest stable SDK so there's nothing to do unless you are looking to install a newer version.

We will ship updates periodically, and you can follow the update procedure below to get those updates.

Please note, the voxl-suite collection of packages is where most of the frequent updates take place. SDK patch releases will not require a system image update, and so most can be gotten by running ```apt update``` and ```apt upgrade``` on the device when connected to the internet. SDK minor and major releases, e.g. from sdk-1.0 to 1.1 will require a system image flash.

If you are having problems with your system, **please** ask questions on the [forum](https://forum.modalai.com/) before reflashing the system image.


### Where To Download

Platform releases can be found on our [Protected Downloads Page](https://developer.modalai.com/asset/2).


## Normal Install Procedure (interactive mode)

**NOTE:** The following commands are all ran on the host computer, not on VOXL 2.

Unzip the download, in this example we'll assume the download name was `voxl2_platform_M.m.b-X.x.tar.gz` where M.m.b is the version of the System Image, and X.x is the version of the VOXL SDK / VOXL Suite

```bash
❯ tar -xvf voxl2_SDK_X.Y.Z.tar.gz
```

Get ready to run the script by going into the directory you just unzipped

```bash
❯ cd voxl2_SDK_X.Y.Z
```

Start the interactive installer:

```bash
./install.sh
```


Now just follow the on-screen instructions. It will walk you through the following main steps:
    - flashing the system image
    - loading voxl-suite
    - setting up the SKU for what product VOXL is installed in
    - configuring MPA services
    - checking for any remaining calibration steps necessary




## Scripted Install Procedure (non-interactive mode)

Non-interactive mode will take the arguments provided to the install script and pass them to voxl-configure-sku at the end of the installation process. This non-interactive mode is meant for scripting or users who are comfortable with the installation process and want a more streamlined installation procedure.

- For non-interactive mode, run the following:
```bash
./install.sh -n/--non-interactive --sku [SKU]
example:
./install.sh -n --sku MRB-D0005-4-V2-C6
```

In this mode, the voxl-configure-sku wizard will not be run, instead the SKU number provided will be written to /data/modalai/sku.txt by the sdk installer script before calling voxl-configure-mpa.

It is your responsibility to make sure the SKU is valid. We recommened using the voxl-configure-sku wizard on a running platform to verify. Afterall, this non-interactive install mode is meant for automated scripted setup for a single known specific platform.

