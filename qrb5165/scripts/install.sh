#!/bin/bash

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is licensed to be used solely in conjunction with devices
#    provided by ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

# Colors, bold, underline, etc
RESET_ALL="\033[0m"
RESET_UNDERLINE="\033[24m"
RESET_BOLD="\033[22m"
SET_BOLD="\033[1m"
SET_UNDERLINE="\033[4m"

CLR_RED="\033[31m"
CLR_YLW="\033[33m"
CLR_GRY="\033[90m"
CLR_GRN="\033[32m"
CLR_LIT_GRN="\033[92m"
CLR_WHT="\033[97m"

DISABLE_WRAP="\033[?7l"
ENABLE_WRAP="\033[?7h"

# Modes and args 
MODE="Interactive"
NON_INTERACTIVE=false
SKIP_SYS_IMG=false
WIPE_PERSISTENT=false
SKIP_MPA_CONFIG=false
KERNEL_OVERRIDE=false

# Platform info
BUILD_NAME=""
BUILD_DATE=""
PLATFORM=""
SYS_IMG_VER=""
SUITE_VER=""
SKU=""

# ------------------------------------------------------- 
# print usage                            
# ------------------------------------------------------- 
_print_usage() {
    echo -e "${CLR_YLW}==============================================================================${RESET_ALL}"
    echo -e ""
    echo -e " This tool flashes VOXL2 SDK which is composed of a system image and voxl-suite."
    echo -e ""
    echo -e "${SET_BOLD}${CLR_GRN}Normal Interactive Usage (recommended):${RESET_ALL}"
    echo -e "${SET_BOLD}${CLR_GRN}    ./install.sh${RESET_ALL}"
    echo -e ""
    echo -e ""
    echo -e "Optional Arguments:"
    echo -e "    -h, --help              show this help message and exit"
    echo -e "    -k, --skip-sys-image    skip installing the system image (for debugging)"
    echo -e "    -n, --non-interactive   don't ask questions, for scripted setup"
    echo -e "    -s, --sku               provide a sku instead of using voxl-configure-sku"
    echo -e "    -w, --wipe-persistent   also wipe the persistent partitions and the parition"
    echo -e "                              table. This is NOT RECOMMEDED!!! it is only for"
    echo -e "                              the case where you have a corrupt parition table."
    echo -e "    -o, --kernel-override   overrides default kernel file with one specified in args"
    echo -e "                               example: ./install.sh -o m0054-1-var00.0-kernel.img"
    echo -e ""
    echo -e "Scripted (non-interactive) example: "
    echo -e "    ./install.sh -n --sku MRB-D0005-4-V2-C6"
    echo -e ""
    echo -e "In this scripted (non-interactive) example, we pass in the product sku and"
    echo -e "the whole install and configuration process will be done for that sku"
    echo -e "without prompting the user. This is for CI and production line use."
    echo -e "${CLR_YLW}==============================================================================${RESET_ALL}"
} 

# ------------------------------------------------------- 
# argument parser
# ------------------------------------------------------- 
_parse_opts() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -h|--help)
                _print_usage
                exit 0
                ;;
            -k|--skip-sys-image)
                echo "skipping system image flash"
                SKIP_SYS_IMG=true
                ;;
            -n|--non-interactive)
                NON_INTERACTIVE=true
                MODE="Non-Interactive"
                ;;
            -s| --sku)
                NON_INTERACTIVE=true
                MODE="Non-Interactive"
                SKU=$2
                shift
                ;;
            -w|--wipe-persistent)
                echo "WARNING: also wiping persistent partitions (/data/)"
                echo "and the parition table!!"
                WIPE_PERSISTENT=true
                ;;
            -m|--skip-mpa)
                echo "skipping MPA configuration"
                SKIP_MPA_CONFIG=true
                ;;
            -o|--kernel-override)
                echo "Overriding default kernel"
                KERNEL_OVERRIDE=true
                if [[ -z "$2" ]] || [[ "$2" =~ ^- ]]; then
                    echo "Error: Argument for $1 is missing" >&2
                    _print_usage
                    exit 1
                fi
                KERNEL_OVERRIDE_FILE="$2"
                shift  # Move past the value to the next option
                ;;
            *)
                echo "invalid arg $1"
                _print_usage
                exit 1
                ;;
        esac
        shift
    done
}

# -------------------------------------------------------
# get platform info
# ------------------------------------------------------- 
_parse_platform_info() {
    if [ -d system-image ]; then
        BUILD_NAME=$(grep "Build_Name" system-image/ver_info.txt | awk '{print $2}' | cut -d '"' -f 2)
        BUILD_DATE=$(grep "Time_Stamp" system-image/ver_info.txt | awk '{print $2}' | cut -d '"' -f 2)
        PLATFORM=$(grep "Platform" system-image/ver_info.txt | awk '{print $2}' | cut -d '"' -f 2)
        SYS_IMG_VER=$(grep "System_Image_Version" system-image/ver_info.txt | awk '{print $2}' | cut -d '"' -f 2)
    fi  

    if [ -d voxl-suite ]; then
        SUITE_VER=$(ls voxl-suite | grep -o "voxl-suite_*.*.*_arm64.deb" | cut -d'_' -f2)
    fi
}

# -------------------------------------------------------
# print welcome message
# ------------------------------------------------------- 
_print_welcome() {
    echo -e $DISABLE_WRAP

    if date | grep -q " Oct 31 " ; then
        # HALLOWEEEEEEEENNNNNN
        echo -e ""
        echo -e "  ███▄ ▄███▓ ▒█████  ▓█████▄  ▄▄▄       ██▓       ${CLR_GRN} ▄▄▄       ██▓${RESET_ALL}"
        echo -e " ▓██▒▀█▀ ██▒▒██▒  ██▒▒██▀ ██▌▒████▄    ▓██▒       ${CLR_GRN}▒████▄    ▓██▒${RESET_ALL}"
        echo -e " ▓██    ▓██░▒██░  ██▒░██   █▌▒██  ▀█▄  ▒██░       ${CLR_GRN}▒██  ▀█▄  ▒██▒${RESET_ALL}"
        echo -e " ▒██    ▒██ ▒██   ██░░▓█▄   ▌░██▄▄▄▄██ ▒██░       ${CLR_GRN}░██▄▄▄▄██ ░██░${RESET_ALL}"
        echo -e " ▒██▒   ░██▒░ ████▓▒░░▒████▓  ▓█   ▓██▒░██████▒   ${CLR_GRN} ▓█   ▓██▒░██░${RESET_ALL}"
        echo -e " ░ ▒░   ░  ░░ ▒░▒░▒░  ▒▒▓  ▒  ▒▒   ▓▒█░░ ▒░▓  ░   ${CLR_GRN} ▒▒   ▓▒█░░▓  ${RESET_ALL}"
        echo -e " ░  ░      ░  ░ ▒ ▒░  ░ ▒  ▒   ▒   ▒▒ ░░ ░ ▒  ░   ${CLR_GRN}  ▒   ▒▒ ░ ▒ ░${RESET_ALL}"
        echo -e " ░      ░   ░ ░ ░ ▒   ░ ░  ░   ░   ▒     ░ ░      ${CLR_GRN}  ░   ▒    ▒ ░${RESET_ALL}"
        echo -e "        ░       ░ ░     ░          ░  ░    ░  ░   ${CLR_GRN}      ░  ░ ░  ${RESET_ALL}"
        echo -e "                      ░                           ${CLR_GRN}              ${RESET_ALL}"
        echo -e ""
    else
        echo -e "                                              ${CLR_GRN}         ${CLR_GRN}  ▂▂▂▂▂▂▂▂▂▂▂▂▂${CLR_GRN}            ${RESET_ALL}"
        echo -e "                                              ${CLR_GRN}      ▂▄▆${CLR_GRN}▆██▛▀▀▀▀▀▀▀▀▜██${CLR_GRN}██▆▆▄▂      ${RESET_ALL}"
        echo -e " ███╗   ███╗ ██████╗ ██████╗  █████╗ ██╗      ${CLR_GRN}   ▗▆████${CLR_GRN}▀▔             ${CLR_GRN}▔▔▀▀▀▀▚▄    ${RESET_ALL}"
        echo -e " ████╗ ████║██╔═══██╗██╔══██╗██╔══██╗██║      ${CLR_GRN} ▗▟████▀ ${CLR_WHT}    ▗██▖    ▐█ ${CLR_GRN}  ▝▀▆▄▄▄    ${RESET_ALL}"
        echo -e " ██╔████╔██║██║   ██║██║  ██║███████║██║      ${CLR_GRN}▟████▀   ${CLR_WHT}   ▗█▘▝█▖   ▐█ ${CLR_GRN}      ▜█▀█▄ ${RESET_ALL}"
        echo -e " ██║╚██╔╝██║██║   ██║██║  ██║██╔══██║██║      ${CLR_GRN}█▌ ▐█▌   ${CLR_WHT}  ▗█▘  ▝█▖  ▐█ ${CLR_GRN}      ▐▄ ▄█ ${RESET_ALL}"
        echo -e " ██║ ╚═╝ ██║╚██████╔╝██████╔╝██║  ██║███████╗ ${CLR_GRN} ▀████   ${CLR_WHT} ▗█▘    ▝█▖ ▐█ ${CLR_GRN}    ▂▄███▀  ${RESET_ALL}"
        echo -e " ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚═╝  ╚═╝╚══════╝ ${CLR_GRN}   ▀▀██▄▄${CLR_GRN}               ${CLR_GRN} ▂▆███▀     ${RESET_ALL}"
        echo -e "                                              ${CLR_GRN}       ▀▀${CLR_GRN}██▄▄ ▀▀▆▄▄▄▄▆██${CLR_GRN}▀▀▀▘        ${RESET_ALL}"
    fi

    echo -e $ENABLE_WRAP
}

# -------------------------------------------------------
# print installation info
# ------------------------------------------------------- 
_print_info() {
    echo -e "${SET_BOLD}Running in ${CLR_LIT_GRN}${SET_UNDERLINE}$MODE${RESET_ALL}${SET_BOLD} mode."
    echo ""
   
    # System Image info
    echo -e "${SET_BOLD}Flashing the following System Image:${RESET_BOLD}"
    echo -e "\tBuild Name: ${CLR_GRN}${SET_BOLD}$BUILD_NAME${RESET_ALL}"
    echo -e "\tBuild Date: ${CLR_GRN}${SET_BOLD}$BUILD_DATE${RESET_ALL}"
    echo -e "\tPlatform: ${CLR_GRN}${SET_BOLD}$PLATFORM${RESET_ALL}"
    echo -e "\tSystem Image Version: ${CLR_GRN}${SET_BOLD}$SYS_IMG_VER${RESET_ALL}"
    if [[ $SKU ]]; then
        echo -e "\tSku: ${CLR_GRN}${SET_BOLD}$SKU${RESET_ALL}"
    fi
    echo ""

    # Voxl-Suite info
    echo -e "${SET_BOLD}Installing the following version of voxl-suite:${RESET_BOLD}"
    echo -e "\tvoxl-suite Version: ${CLR_GRN}${SET_BOLD}$SUITE_VER${RESET_ALL}"
    echo ""

    echo -e "${RESET_ALL}"
}



# -------------------------------------------------------
# Actual start of execution
# -------------------------------------------------------
_parse_opts $@
_parse_platform_info
_print_welcome
_print_info

if [ $NON_INTERACTIVE == false ]; then
    echo -e "${SET_BOLD}${CLR_LIT_GRN}Would you like to continue with SDK install?${RESET_ALL}"
    select opt in "Yes" "No"; do
        case $opt in
            "Yes" )
                break;;
            "No" )
                echo -e "${CLR_GRN}[INFO] Exiting SDK install.${RESET_ALL}"
                exit 0
                ;;
            *)
                echo -e "${CLR_RED}[ERROR] invalid option${RESET_ALL}"
                ;;
        esac
    done
fi


FILE=$(which adb)
if [ -f "$FILE" ]; then
    echo -e "${CLR_GRN}[INFO] adb installed${RESET_ALL}"
else
    echo ""
    echo -e "${CLR_RED}${SET_BOLD}[Error]: android-tools-adb missing, use: apt-get install android-tools-adb${RESET_ALL}"
    echo ""
    exit 1
fi

FILE=$(which fastboot)
if [ -f "$FILE" ]; then
    echo -e "${CLR_GRN}[INFO] fastboot installed${RESET_ALL}"
    echo ""
else
    echo ""
    echo -e "${CLR_RED}${SET_BOLD}[Error]: android-tools-fastboot missing, use: apt-get install android-tools-fastboot${RESET_ALL}"
    echo ""
    exit 1
fi

set -e

# verify folders
if ! [ -d system-image ]; then
    echo ""
    echo -e "${CLR_RED}${SET_BOLD}[Error]: missing system-image directory${RESET_ALL}"
    echo ""
    exit 1
fi

if ! [ -d voxl-suite ]; then
    echo ""
    echo -e "${CLR_RED}${SET_BOLD}[Error]: missing voxl-suite directory${RESET_ALL}"
    echo ""
    exit 1
fi

if ! $SKIP_SYS_IMG; then
    FLASH_ARGS=""
    if $NON_INTERACTIVE; then
        FLASH_ARGS="--non-interactive"
    fi
    if $WIPE_PERSISTENT; then
        FLASH_ARGS="$FLASH_ARGS -f"
        echo -e "${CLR_LIT_GRN}${SET_BOLD}----doing full parition table wipe----${RESET_ALL}"
    fi
    if $KERNEL_OVERRIDE; then
        FLASH_ARGS="$FLASH_ARGS -k $KERNEL_OVERRIDE_FILE"
        echo -e "${CLR_LIT_GRN}${SET_BOLD}----overriding kernel with $KERNEL_OVERRIDE_FILE----${RESET_ALL}"
    fi
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----  Starting System Image Flash ----${RESET_ALL}"
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----./flash-system-image.sh $FLASH_ARGS ----${RESET_ALL}"

    cd system-image

    ./flash-system-image.sh $FLASH_ARGS


    if [ "$?" -ne 0 ]; then
        echo -e "${CLR_RED}${SET_BOLD}[ERROR] Exiting${RESET_ALL}"
        exit 1
    fi

    cd ..
fi

adb wait-for-device

if [ -d pre-tweaks ]; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Applying Pre Tweaks----${RESET_ALL}"
    cd pre-tweaks
    ./apply_tweaks.sh
    cd ..
fi

echo -e "${CLR_LIT_GRN}${SET_BOLD}----Installing Suite----${RESET_ALL}"
cd voxl-suite
./offline-install.sh
cd ..


## now that modalai-slpi is in voxl-suite instead of the system image
## we need to reboot after installing it to make sure the new slpi image is
## loaded before running voxl-configure-mpa which loads ESC and ELRS firmware
echo -e "${CLR_LIT_GRN}${SET_BOLD}----Rebooting VOXL----${RESET_ALL}"
adb reboot
adb wait-for-device

echo -e "${CLR_LIT_GRN}${SET_BOLD}----Installation Completed----${RESET_ALL}"


if [ -d post-tweaks ]; then
    echo -e "${CLR_LIT_GRN}${SET_BOLD}----Applying Post Tweaks----${RESET_ALL}"
    cd post-tweaks
    ./apply_tweaks.sh
    cd ..
fi

if ! $SKIP_MPA_CONFIG; then
    ## if a sku was provided just use it
    if  [ $SKU ]; then
        echo -e "[INFO] Provided sku: $SKU"
        adb shell "echo $SKU > /data/modalai/sku.txt"
    ## otherwise pull up the wizard in interactive mode only
    elif [ $NON_INTERACTIVE == false ]; then
        printf '\33[H\33[2J'
        adb shell voxl-configure-sku --wizard
    else
        echo "[INFO] no --sku argument provided"
        echo "continuing with the existing SKU on VOXL"
    fi

    ## set up wifi
    if [ $NON_INTERACTIVE == false ]; then
        adb shell voxl-wifi wizard
    else
        echo -e "[INFO] Setting WiFi to factory factory softap mode"
        adb shell voxl-wifi factory
    fi

    ## now configure mpa, always in non-interactive mode since it just
    ## reads the SKU and asks to continue otherwise.
    printf '\33[H\33[2J'
    adb shell voxl-configure-mpa --non-interactive
fi
#echo -e "${CLR_GRN}SDK Install Completed${RESET_ALL}"
